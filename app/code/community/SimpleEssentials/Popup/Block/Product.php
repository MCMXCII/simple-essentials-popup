<?php

/**
 *  Simple Essentials - Popup
 *
 * @category    Magento Module
 * @package     SimpleEssentails_Popup
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      MCMXCII <dominic.sutton@djwd.co.uk>
 */

class SimpleEssentials_Popup_Block_Product extends Mage_Core_Block_Template
{
    const DEFAULT_TITLE = 'Customers who viewed this also viewed these products:';

    /**
     * Get Product Identifier
     *
     * @return boolean
     */
    private function _getProductIdentifier()
    {
        $productIdentifier = Mage::getSingleton('core/session')->getPopupProduct();
        Mage::getSingleton('core/session')->unsetData('popup_product');
        return $productIdentifier;
    }

    /**
     * Get Popup HTML
     *
     * @return HTML
     */
    public function getPopupHtml()
    {
        $html = '';
        if (Mage::helper('simpleessentials_popup')->isEnabled() && $productIdentifier = $this->_getProductIdentifier()) {
            $product = Mage::getModel('catalog/product')->load($productIdentifier);
            $html .= $this->getLayout()
            ->createBlock('simpleessentials_popup/product')
            ->setTemplate('simpleessentials/popup/popup.phtml')
            ->setProduct($product)
            ->toHtml();
        }
        return $html;
    }

    /**
     * Output Relevant Popup
     *
     * @return HTML
     */
    public function getProducts()
    {
        $product = $this->getProduct();
        $return = [];
        switch (Mage::getStoreConfig('simple_essentials_popup/settings/product_set')) {
            case 'upsell': $ids = $product->getUpSellProductIds();break;
            case 'cross': $ids = $product->getCrossSellProductIds();break;
            case 'related': $ids = $product->getRelatedProductIds();break;
            default: $ids = $product->getRelatedProductIds();break;
        }

        foreach ($ids as $id) {
            $return[] = Mage::getModel('catalog/product')->load($id);
        }
        return $return;
    }

    /**
     * Get Title
     *
     * @return  String
     */
    public function getTitle()
    {
        return Mage::getStoreConfig('simple_essentials_popup/settings/title') != '' ? Mage::getStoreConfig('simple_essentials_popup/settings/title') : self::DEFAULT_TITLE;
    }
}