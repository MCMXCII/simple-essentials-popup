<?php

/**
 *  Simple Essentials - Popup
 *
 * @category    Magento Module
 * @package     SimpleEssentails_Popup
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      MCMXCII <dominic.sutton@djwd.co.uk>
 */

class SimpleEssentials_Popup_Model_Observer
{
    /**
     * Trigger Popup
     *
     * Set the command to trigger the popup into the session
     */
    public function triggerPopup(Varien_Event_Observer $observer)
    {
        if (Mage::helper('simpleessentials_popup')->isEnabled()) {
            Mage::getSingleton('core/session')->setPopupProduct($observer->getProduct()->getId());
        }
    }
}