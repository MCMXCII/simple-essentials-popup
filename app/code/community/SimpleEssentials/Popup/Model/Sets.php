<?php

/**
 *  Simple Essentials - Popup
 *
 * @category    Magento Module
 * @package     SimpleEssentails_Popup
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      MCMXCII <dominic.sutton@djwd.co.uk>
 */

class SimpleEssentials_Popup_Model_Sets
{
    public function toOptionArray()
    {
        return array(
            array('value'=>'related', 'label'=>Mage::helper('simpleessentials_popup')->__('Related Products')),
            array('value'=>'cross', 'label'=>Mage::helper('simpleessentials_popup')->__('Cross Sell Products')),
            array('value'=>'upsell', 'label'=>Mage::helper('simpleessentials_popup')->__('Up Sell Products')),
        );
    }
}