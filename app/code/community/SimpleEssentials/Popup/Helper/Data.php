<?php

/**
 *  Simple Essentials - Popup
 *
 * @category    Magento Module
 * @package     SimpleEssentails_Popup
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      MCMXCII <dominic.sutton@djwd.co.uk>
 */

class SimpleEssentials_Popup_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function isEnabled()
    {
        return Mage::getStoreConfig('simple_essentials_popup/settings/enabled') == '1' ? true : false;
    }
}